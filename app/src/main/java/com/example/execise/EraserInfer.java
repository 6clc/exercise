package com.example.execise;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
public class EraserInfer {

    // Init NCNN Model
    public native boolean Init(AssetManager mgr);

    // Use NCNN to infer
    public native String Detect(Bitmap bitmap, boolean use_gpu);

    static {
        System.loadLibrary("eraser_infer");
    }
}
