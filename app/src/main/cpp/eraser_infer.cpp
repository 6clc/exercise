#include <android/asset_manager_jni.h>
#include <android/bitmap.h>
#include <android/log.h>

#include <jni.h>

#include <string>
#include <vector>

// ncnn
#include "layer.h"
#include "net.h"
#include "benchmark.h"

static ncnn::UnlockedPoolAllocator g_blob_pool_allocator;
static ncnn::PoolAllocator g_workspace_pool_allocator;

static ncnn::Net eraser_infer;

extern "C" {

// FIXME DeleteGlobalRef is missing for objCls
static jclass objCls = NULL;
static jmethodID constructortorId;
static jfieldID xId;
static jfieldID yId;
static jfieldID wId;
static jfieldID hId;
static jfieldID labelId;
static jfieldID probId;

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    __android_log_print(ANDROID_LOG_DEBUG, "Eraser NCNN", "JNI_OnLoad");

    //  learn gpu of android
    // ncnn::create_gpu_instance();

    return JNI_VERSION_1_4;
}

JNIEXPORT void JNI_OnUnload(JavaVM* vm, void* reserved)
{
    __android_log_print(ANDROID_LOG_DEBUG, "Eraser NCNN", "JNI_OnUnload");
    //  learn gpu of android
//    ncnn::destroy_gpu_instance();
}

// public native boolean Init(AssetManager mgr);
JNIEXPORT jboolean
Java_com_example_execise_EraserInfer_Init(JNIEnv* env, jobject thiz, jobject assetManager)
{
    ncnn::Option opt;
    opt.lightmode = true;
    opt.num_threads = 4;
    opt.blob_allocator = &g_blob_pool_allocator;
    opt.workspace_allocator = &g_workspace_pool_allocator;
    opt.use_packing_layout = true;
    // TODO(6clc): use float16 to compute
//    opt.use_fp16_packed = false;
//    opt.use_fp16_arithmetic = false;
//    opt.use_fp16_storage = false;


    // use vulkan compute
    // TODO(6clc): use gpu on mobile to compute
//    if (ncnn::get_gpu_count() != 0)
//        opt.use_vulkan_compute = true;

    AAssetManager* mgr = AAssetManager_fromJava(env, assetManager);

    eraser_infer.opt = opt;


    // init param
    {
        int ret = eraser_infer.load_param(mgr, "generate.param");
        if (ret != 0)
        {
            __android_log_print(ANDROID_LOG_DEBUG, "Eraser NCNN", "load_param failed");
            return JNI_FALSE;
        }
__android_log_print(ANDROID_LOG_DEBUG, "Eraser NCNN", "load_param success");
    }

    // init bin
    {
        int ret = eraser_infer.load_model(mgr, "generate.bin");
        if (ret != 0)
        {
            __android_log_print(ANDROID_LOG_DEBUG, "Eraser NCNN", "load_model failed");
            return JNI_FALSE;
        }
__android_log_print(ANDROID_LOG_DEBUG, "Eraser NCNN", "load_model success");
    }

    return JNI_TRUE;
}

// public native Bitmap Detect(Bitmap bitmap, boolean use_gpu);
JNIEXPORT jstring
Java_com_example_execise_EraserInfer_Detect(JNIEnv* env, jobject thiz, jobject bitmap, jboolean use_gpu)
{
    // TODO
//    if (use_gpu == JNI_TRUE && ncnn::get_gpu_count() == 0)
//    {
//        return NULL;
//    }

    double start_time = ncnn::get_current_time();

    AndroidBitmapInfo info;
    AndroidBitmap_getInfo(env, bitmap, &info);
    const int width = info.width;
    const int height = info.height;
    if (info.format != ANDROID_BITMAP_FORMAT_RGBA_8888)
        return NULL;

    // ncnn from bitmap
    const int target_size = 512;

    // letterbox pad to multiple of 32
    int w = width;
    int h = height;
    float scale = 1.f;
    if (w > h)
    {
        scale = (float)target_size / w;
        w = target_size;
        h = h * scale;
    }
    else
    {
        scale = (float)target_size / h;
        h = target_size;
        w = w * scale;
    }

    ncnn::Mat in = ncnn::Mat::from_android_bitmap_resize(env, bitmap, ncnn::Mat::PIXEL_RGB, w, h);

    const float norm_vals[3] = {1 / 255.f, 1 / 255.f, 1 / 255.f};
    in.substract_mean_normalize(0, norm_vals);
    __android_log_print(ANDROID_LOG_DEBUG, "in pre process", "%f", in[0]);


    ncnn::Mat in2(w, h);
//    in2.fill<float>(1./255.);
    
    ncnn::Extractor ex = eraser_infer.create_extractor();
    ex.input(0, in);
//    ex.input(1, in2);
    ncnn::Mat out;
    ex.extract("420", out);
    __android_log_print(ANDROID_LOG_DEBUG, "out pre process", "%f", out[0]);
    const float norm_vals2[3] = {255.f, 255.f, 255.f};
    out.substract_mean_normalize(0, norm_vals2);
   __android_log_print(ANDROID_LOG_DEBUG, "out after process", "%f", out[0]);
    out.to_android_bitmap(env, bitmap, ncnn::Mat::PIXEL_RGB);


    double elasped = ncnn::get_current_time() - start_time;
    __android_log_print(ANDROID_LOG_DEBUG, "Eraser", "%.2fms   detect", elasped);

    jstring result = env->NewStringUTF("success");
    return result;
    }

}
